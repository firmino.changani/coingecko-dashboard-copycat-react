# 1.0.0 (2021-09-02)


### Bug Fixes

* add gl token ([7a41a93](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/7a41a93e5960642e5cf91e062fdb947b3090466a))
* add runner to sonarqube job ([39f04f5](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/39f04f55cf74199aa1f8ed786985ae1a715e3cd1))
* adjust commit lint rules ([cb967b1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/cb967b14b2c4b96945bba1890e632a4349fc646d))
* enable artifacts ([f7e79bb](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/f7e79bba51f46e5b3708420126f34698d8a6d2c2))
* enable docker image ([d333de1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/d333de131d1beff4d0a9db28de323cde6bb18ca9))
* enable stages ([d028ab4](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/d028ab42c14856cf22959e65d0fd6a85bc319476))
* purge git cache ([bf5045e](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/bf5045e75d2af6b5d4b8dfd7ceb2944ab0af8539))
* remove cd frontend ([e551f77](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/e551f773963924429705e97f2dea90e90be34a54))
* remove deploy jobs ([12dfe11](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/12dfe11644037fecd037bcb85d0b7594415b6416))
* specify pipeline runner ([14a7ad7](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/14a7ad7e13e7ec2ad72297716ab5c3f35abdaaa5))
* update package-lock ([78b00a0](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/78b00a0c17a8f09d194b2fa361afbe883e4fffaa))
* use shared runners ([3854a75](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/3854a7508c1b9f4072baae1d0d6b20d2cf4d1ee9))


### Features

* [wip] add view to render a single exchange ([c48a33e](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/c48a33ef63a778a26e9bfac4265a0b4093e6dafe))
* add exchanges module ([4e37821](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/4e37821b4ad3106d75c7c7c70b8dc26f3ea1c43b))
* add header component ([c190dd6](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/c190dd6b3e57e3ea7ef2788f1ba5329fdef9beed))
* Add pipeline ([9cf19ce](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/9cf19ce5dae881b9c3dbc5c3df8ea634b33ad2a1))
* add trust score component ([4e1796a](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/4e1796a1c2e53480c768f47d61bfcf0c994c0602))
* add util to conver number into currency ([449a067](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/449a0679acd7065b672b4be2e104f039c5bbeac7))
* add view title component ([5850879](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/58508798655a4c48eed3061b8f856fa9b1d38040))
* cache single exchange ([c7bcf7f](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/c7bcf7f24785ca34360ec156603b36c676837f26))
* create data-table component ([7c35679](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/7c35679c329e47090ee2f1228305792ac8b4f93d))
* extract functions to an util module ([54492b7](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/54492b7aa0051e4d0599889447e44614873c06d0))
* register ListExchanges view ([82dbc05](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/82dbc058d1167076bc27f98af850a28591727d06))
* Style table name ([e9adbf2](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/e9adbf2e1cd7ad3f04e623dbd9d6af7b8309e7be))
* WIP ([4cfeabb](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/4cfeabbd7a4b50f8fe0ca0a4a9c44bac3207efaf))
* **#1:** Add dummy login ([8e3d14a](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/8e3d14acbd903a97c0a725762c3b5f1100b2d6e8)), closes [#1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/issues/1)
* **#1:** Define routing strategy ([ea292d9](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/ea292d910f9b0e11d327c262e283c90c458aeea0)), closes [#1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/issues/1)
* **#1:** Install dependencies ([30637e6](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/30637e60da6c608d02c5820cf6701ced089a2ccb)), closes [#1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/issues/1)
* **#1:** Setup redux ([c6767c4](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/commit/c6767c414a2bfda70e6faefb3ccda667d7c237b5)), closes [#1](https://gitlab.com/firmino.changani/coingecko-dashboard-copycat-react/issues/1)
