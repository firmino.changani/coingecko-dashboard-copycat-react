# CoinGecko Dashboard Copycat

## Architecture

- `./common`: It contains the backbone of the whole application, including views, components, utils, styles, and any generic asset or script needed to assemble the application.
- `modules`: It contains the implementation of the use cases bounded to the application.
- `app entry point`: It contains all the entry point scripts and components of the application.

## Project setup

- ✅ CI - Gitlab CI
- ✅ Commit linter - [Commitlint](https://commitlint.js.org/#/)
- ✅ Semantic releasesa and auto-versioning (semver) - [semantic-release](https://semantic-release.gitbook.io/semantic-release/)
- ✅ Static code analysis - [Sonarqube](https://sonarqube.changani.me/dashboard?id=firmino.changani_coingecko-dashboard-copycat-react_AXudqQ52pEE4tfepoahF)
- ⚠️ Lighthouse analysis

## Environments

- Production `main` - [coingecko-dashboard-react.changani.me](https://coingecko-dashboard-react.changani.me)
- Development `develop` - [dev.coingecko-dashboard-react.changani.me](https://dev.coingecko-dashboard-react.changani.me)
